﻿using System;
namespace WastedAPI.Models
{
    public class Product : ProductBaseInfo
    {
        /// <summary>
        /// Date when product was bought / added to app.
        /// </summary>
        public DateTime BuyDate { get; set; }

        /// <summary>
        /// How much of the product left.
        /// </summary>
        public double RemainingAmount { get; set; }

        /// <summary>
        /// Unit of the remaining amount (e.g. kilograms, mililiters, kutasosankis etc).
        /// </summary>
        public string Unit { get; set; }
    }
}
