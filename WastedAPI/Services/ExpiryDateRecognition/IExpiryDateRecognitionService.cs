﻿using System;
namespace WastedAPI.Services.ExpiryDateRecognition
{
    public interface IExpiryDateRecognitionService
    {
        DateTime GetExpiryDate(byte[] imageBytes);
    }
}
