﻿using System;

namespace WastedAPI.DataAccess.User
{
    public interface IUserDataAccess
    {
        DateTime NotificationTime { get; set; }

        int Points { get; set; }
    }
}
