﻿namespace WastedAPI.Services.BarcodeRecognition
{
    public interface IBarcodeRecognitionService
    {
        string GetBarcodeFromImage(byte[] imageBytes);
    }
}
