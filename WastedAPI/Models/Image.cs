﻿namespace WastedAPI.Models
{
    public class Image
    {
        public string Base64Bytes { get; set; }
    }
}
