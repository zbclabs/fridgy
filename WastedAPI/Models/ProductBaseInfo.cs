﻿using System;
namespace WastedAPI.Models
{
    public class ProductBaseInfo
    {
        /// <summary>
        /// GUID identificator.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Product category (e.g. fruits).
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Product name (e.g. banana).
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Date after which product expires.
        /// </summary>
        public DateTime ExpiryDate { get; set; }
    }
}
