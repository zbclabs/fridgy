﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Google.Cloud.Vision.V1;
using Grpc.Core;

namespace WastedAPI.Services.ExpiryDateRecognition
{
    public class ExpiryDateRecognitionService : IExpiryDateRecognitionService
    {
        public DateTime GetExpiryDate(byte[] imageBytes)
        {
            var builder = new ImageAnnotatorClientBuilder();
            builder.CredentialsPath = "api_key.json";
            var client = builder.Build();

            var image = Image.FromBytes(imageBytes);
            var response = client.DetectText(image);

            return ProcessOutput(response.Select(annotation => annotation.Description));
        }

        private DateTime ProcessOutput(IEnumerable<string> recognizedStrings)
        {
            var ordered = recognizedStrings
                .Select(ParseDateTimeString)
                .Where(date => date != null)
                .Select(date => date.Value)
                .OrderByDescending(date => date);

            return ordered.SingleOrDefault();
        }

        private static DateTime? ParseDateTimeString(string dateTimeString)
        {
            try
            {
                var result = Convert.ToDateTime(dateTimeString, new CultureInfo("pl-PL"));
                return result;
            }
            catch (Exception exception)
            {
                return null;
            }
        }
    }
}
