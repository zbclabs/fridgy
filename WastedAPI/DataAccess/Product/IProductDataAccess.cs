﻿using System.Collections.Generic;
using ProductModel = WastedAPI.Models.Product;

namespace WastedAPI.DataAccess.Product
{
    public interface IProductDataAccess
    {
        List<ProductModel> Products { get; }

        void Reset();
    }
}
