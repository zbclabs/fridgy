﻿using System.Drawing;
using System.IO;
using ZXing;

namespace WastedAPI.Services.BarcodeRecognition
{
    public class BarcodeRecognitionService : IBarcodeRecognitionService
    {
        private readonly IBarcodeReader barcodeReader = new BarcodeReader();

        public string GetBarcodeFromImage(byte[] imageBytes)
        {
            var bitmap = new Bitmap(new MemoryStream(imageBytes));
            var bitmapLuminanceSource = new BitmapLuminanceSource(bitmap);
            var decodingResult = barcodeReader.Decode(bitmapLuminanceSource);

            return decodingResult != null
                ? decodingResult.Text
                : null;
        }
    }
}
