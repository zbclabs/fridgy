﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace WastedAPI.Controllers
{
    [Route("api/[controller]")]
    public class CustomUsagesController : Controller
    {
        /// <summary>
        /// Get all custom usages defined by user.
        /// </summary>
        // GET: api/customUsages
        [HttpGet]
        public IEnumerable<object> GetAllUsages()
        {
            return new List<string>
            {
                "Usage1",
                "Usage2",
                "Usage3"
            };
        }

        /// <summary>
        /// Add new custom usage.
        /// </summary>
        /// <param name="usage"></param>
        /// <returns></returns>
        // POST: api/customUsages
        [HttpPost]
        public bool AddNewUsage([FromBody] object usage)
        {
            return true;
        }

        /// <summary>
        /// Edit usage with provided ID.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="usage"></param>
        /// <returns></returns>
        // PUT: /api/customUsages/{id}
        [HttpPut("{id}")]
        public bool EditUsage(int id, [FromBody] object usage)
        {
            return true;
        }

        /// <summary>
        /// Delete usage with provided ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: /api/customUsages/{id}
        [HttpDelete("{id}")]
        public bool DeleteUsage(int id)
        {
            return true;
        }
    }
}
