﻿using System.Collections.Generic;
using WastedAPI.Models;
using ProductModel = WastedAPI.Models.Product;

namespace WastedAPI.DataAccess.Product
{
    public class ProductDataAccess : IProductDataAccess
    {
        private static List<ProductModel> products = FakeDatabase.FillTheFridge();

        public List<ProductModel> Products => products;

        public void Reset()
        {
            products = FakeDatabase.FillTheFridge();
        }
    }
}
