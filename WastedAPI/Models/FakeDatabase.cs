﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace WastedAPI.Models
{
    public static class FakeDatabase
    {
        public static List<T> GetMockListOfSelectedType<T>(string dbFileLocation) where T : class
        {
            var text = File.ReadAllText(dbFileLocation);
            return JsonConvert.DeserializeObject<List<T>>(text);
        }

        public static List<BarCodeProductAssociation> GetListOfBarCodesProductAssociations()
        {
            return GetMockListOfSelectedType<BarCodeProductAssociation>("barcode_db.json");
        }

        public static List<Product> FillTheFridge()
        {
            return GetMockListOfSelectedType<Product>("products_db.json");
        }

        public static List<Recipe> GetRecipes()
        {
            return GetMockListOfSelectedType<Recipe>("recipies_db.json");
        }
    }

    public class BarCodeProductAssociation
    {
        public string BarCode { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
    }
}
