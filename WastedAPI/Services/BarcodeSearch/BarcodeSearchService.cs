﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using WastedAPI.Models;

namespace WastedAPI.Services.BarcodeSearch
{
    public class ReturnedProduct
    {
        public List<string> categories_hierarchy { get; set; }
        public string product_name { get; set; }
    }

    public class BarcodeResponse
    {
        public ReturnedProduct product { get; set; }
    }

    public class BarcodeSearchService : IBarcodeSearchService
    {
        public Product SearchByBarcode(string barcode)
        {
            try
            {
                var client = new HttpClient();
                var response = client.GetAsync("https://world.openfoodfacts.org/api/v0/product/" + barcode + ".json").Result;
                response.EnsureSuccessStatusCode();
                string responseBody = response.Content.ReadAsStringAsync().Result;
                var parsedResponse = JsonConvert.DeserializeObject<BarcodeResponse>(responseBody);

                return new Product
                {
                    Name = parsedResponse.product.product_name,
                    Category = GetCategory(parsedResponse.product.categories_hierarchy)
                };
            }
            catch (Exception exception)
            {
                return null;
            }
        }

        private string GetCategory(List<string> categories)
        {
            var suitableCategory = categories
                .FirstOrDefault(category => category.Contains("pl:"))
                    ?? categories.FirstOrDefault();

            return RemovePrefix(suitableCategory);
        }

        private static string RemovePrefix(string category)
        {
            var colonIndex = category.IndexOf(':');

            if (colonIndex == -1)
                return category;

            return category.Substring(colonIndex + 1);
        }
    }
}
