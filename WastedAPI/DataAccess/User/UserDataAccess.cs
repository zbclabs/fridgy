﻿using System;

namespace WastedAPI.DataAccess.User
{
    public class UserDataAccess : IUserDataAccess
    {
        private static DateTime notificationTime = DateTime.Now;
        private static int points;

        public DateTime NotificationTime
        {
            get => notificationTime;
            set => notificationTime = value;
        }

        public int Points
        {
            get => points;
            set => points = value;
        }
    }
}
