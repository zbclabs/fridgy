﻿using System.Collections.Generic;
using System.Linq;
using WastedAPI.Models;
using WastedAPI.Services.BarcodeRecognition;
using WastedAPI.Services.BarcodeSearch;

namespace WastedAPI.Services.ProductFromImage
{
    public class ProductFromImageService : IProductFromImageService
    {
        private static readonly Product defaultProduct = new Product
        {
            Category = "Fruits",
            Name = "Banana"
        };

        private readonly IBarcodeRecognitionService barcodeRecognitionService = new BarcodeRecognitionService();
        private readonly IBarcodeSearchService barcodeSearchService = new BarcodeSearchService();
        private readonly List<BarCodeProductAssociation> barcodeProductAssociations;

        public ProductFromImageService()
        {
            barcodeProductAssociations = FakeDatabase.GetListOfBarCodesProductAssociations();
        }

        public Product GetProductFromImage(byte[] imageBytes)
        {
            var recognizedBarcode = barcodeRecognitionService.GetBarcodeFromImage(imageBytes);

            if (recognizedBarcode == null)
            {
                return defaultProduct;
            }

            var foundProduct = barcodeSearchService.SearchByBarcode(recognizedBarcode);

            if (foundProduct != null)
            {
                return foundProduct;
            }

            var associatedProduct = barcodeProductAssociations
                .SingleOrDefault(association => association.BarCode == recognizedBarcode);

            return associatedProduct != null
                ? new Product
                {
                    Category = associatedProduct.Category,
                    Name = associatedProduct.Name
                } : defaultProduct;
        }
    }
}
