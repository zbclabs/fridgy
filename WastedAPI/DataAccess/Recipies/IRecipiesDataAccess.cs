﻿using System.Collections.Generic;
using WastedAPI.Models;

namespace WastedAPI.DataAccess.Recipies
{
    public interface IRecipiesDataAccess
    {
        List<Recipe> Recipies { get; }

        void Reset();
    }
}
