﻿using WastedAPI.Models;

namespace WastedAPI.Services.ProductFromImage
{
    public interface IProductFromImageService
    {
        Product GetProductFromImage(byte[] imageBytes);
    }
}
