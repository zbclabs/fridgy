﻿namespace WastedAPI.Models
{
    public class Ingredient
    {
        /// <summary>
        /// Product to use.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Amount of product to use.
        /// </summary>
        public double Amount { get; set; }

        /// <summary>
        /// Unit of amount of product to use.
        /// </summary>
        public string Unit { get; set; }
    }
}
