﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WastedAPI.DataAccess.Product;
using WastedAPI.Models;
using WastedAPI.Services.BarcodeSearch;
using WastedAPI.Services.ExpiryDateRecognition;
using WastedAPI.Services.ProductFromImage;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WastedAPI.Controllers
{
    /// <summary>
    /// Product controller
    /// </summary>
    [Route("api/[controller]")]
    public class ProductsController : Controller
    {
		private readonly IExpiryDateRecognitionService expiryDateRecognitionService = new ExpiryDateRecognitionService();
        private readonly IProductDataAccess productDataAccess = new ProductDataAccess();
        private readonly IProductFromImageService productFromImageService = new ProductFromImageService();

		public IExpiryDateRecognitionService ExpiryDateRecognitionService => expiryDateRecognitionService;

		/// <summary>
		/// Get all products in the fridge.
		/// </summary>
		/// <returns></returns>
		// GET: api/products
		[HttpGet]
        public IEnumerable<ProductBaseInfo> GetAllProducts()
        {
            return productDataAccess.Products;
        }

        /// <summary>
        /// Get detailed product with provided ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/products/{id}
        [HttpGet("{id}")]
        public Product GetProductById(string id)
        {
            return productDataAccess.Products
                .SingleOrDefault(product => product.Id == id);
        }

        /// <summary>
        /// Add product to the fridge.
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        // POST: api/products
        [HttpPost]
        public bool AddProduct([FromBody] Product product)
        {
            productDataAccess.Products.Add(product);
            return true;
        }

        /// <summary>
        /// Send photo to recognize product
        /// </summary>
        /// <param name="photo">base64 encoded photo</param>
        /// <returns>Product</returns>
        [HttpPost("recognizeFromPhoto")]
        public object RecognizeProduct([FromBody] Image photo)
        {
            return productFromImageService.GetProductFromImage(Convert.FromBase64String(photo.Base64Bytes));
        }

		/// <summary>
		/// Send photo to recognize expiry date
		/// </summary>
		/// <param name="photo">base64 encoded photo</param>
		/// <returns></returns>
		[HttpPost("recognizeExpiryDate")]
		public string Get([FromBody] Image photo)
		{
			return expiryDateRecognitionService.GetExpiryDate(Convert.FromBase64String(photo.Base64Bytes)).ToString();
		}

		/// <summary>
		/// Delete product from the fridge.
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		// DELETE: api/products/{id}
		[HttpDelete("{id}")]
        public bool DeleteProduct(string id)
        {
            var product = productDataAccess.Products
                .SingleOrDefault(p => p.Id == id);

            if (product == null)
                return false;

            productDataAccess.Products.Remove(product);
            return true;
        }

        /// <summary>
        /// Use product with provided ID (e.g. use 500 mL of milk).
        /// </summary>
        /// <param name="id"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        // POST: api/products/{id}/use
        [HttpPost("{id}/use")]
        public bool UseProduct(string id, [FromBody] double amount)
        {
            var product = productDataAccess.Products
                .SingleOrDefault(p => p.Id == id);

            if (product == null)
                return false;

            product.RemainingAmount -= amount;
            return true;
        }

        [HttpPost("testBarcode/{barcode}")]
        public Product TestBarcode(string barcode)
		{
			var service = new BarcodeSearchService();
			return service.SearchByBarcode(barcode);
		}
    }
}
