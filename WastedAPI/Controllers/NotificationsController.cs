﻿using System;
using Microsoft.AspNetCore.Mvc;
using WastedAPI.DataAccess.User;

namespace WastedAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationsController : ControllerBase
    {
        private readonly IUserDataAccess userDataAccess = new UserDataAccess();

        /// <summary>
        /// Get the time when system notification should be sent to user.
        /// </summary>
        /// <returns></returns>
        //GET: api/Notifications/time/
        [HttpGet("time")]
        public string GetTime()
        {
            return userDataAccess.NotificationTime.ToString();
        }

        /// <summary>
        /// Update the time when system notification should be sent to user.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        //PUT: api/Notifications/time/5
        [HttpPut("time")]
        public bool PutTime([FromBody] string value)
        {
            userDataAccess.NotificationTime = DateTime.Parse(value);
            return true;
        }
    }
}
