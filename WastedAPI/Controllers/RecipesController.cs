﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WastedAPI.DataAccess.Product;
using WastedAPI.DataAccess.Recipies;
using WastedAPI.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WastedAPI.Controllers
{
    [Route("api/[controller]")]
    public class RecipesController : Controller
    {
        private readonly IProductDataAccess productDataAccess = new ProductDataAccess();
        private readonly IRecipiesDataAccess recipiesDataAccess = new RecipiesDataAccess();

        /// <summary>
        /// Get recipes to display to user.
        /// </summary>
        /// <returns></returns>
        // GET: api/recipes
        [HttpGet]
        public IEnumerable<Recipe> GetRecipes()
        {
            var productNames = productDataAccess.Products
                .Select(product => product.Name)
                .ToList();

            return recipiesDataAccess.Recipies
                .OrderByDescending(recipe => recipe.Ingredients
                    .Select(ingredient => ingredient.Name)
                    .Count(ingredientName => productNames
                        .Contains(ingredientName)));
        }
    }
}
