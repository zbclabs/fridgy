﻿using System;
using Microsoft.AspNetCore.Mvc;
using WastedAPI.DataAccess.Product;
using WastedAPI.DataAccess.Recipies;
using WastedAPI.DataAccess.User;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WastedAPI.Controllers
{
    [Route("api/[controller]")]
    public class ResetController : Controller
    {
        private readonly IProductDataAccess productDataAccess = new ProductDataAccess();
        private readonly IUserDataAccess userDataAccess = new UserDataAccess();
        private readonly IRecipiesDataAccess recipiesDataAccess = new RecipiesDataAccess();

        /// <summary>
        /// Reset API state.
        /// </summary>
        /// <returns></returns>
        // POST: api/reset
        [HttpPost]
        public bool ResetApiState()
        {
            productDataAccess.Reset();
            userDataAccess.NotificationTime = DateTime.Now;
            userDataAccess.Points = 0;
            recipiesDataAccess.Reset();
            return true;
        }
    }
}
