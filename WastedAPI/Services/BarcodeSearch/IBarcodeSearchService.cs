﻿using WastedAPI.Models;

namespace WastedAPI.Services.BarcodeSearch
{
    public interface IBarcodeSearchService
    {
        Product SearchByBarcode(string barcode);
    }
}
