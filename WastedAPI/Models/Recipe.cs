﻿using System.Collections.Generic;

namespace WastedAPI.Models
{
    public class Recipe
    {
        /// <summary>
        /// Recipe's GUID identificator.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Recipe's title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Recipe's description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Recipe's ingredients.
        /// </summary>
        public List<Ingredient> Ingredients { get; set; }

        /// <summary>
        /// Recipe's steps. String Split (".") to get list of steps, "." is a separator
        /// </summary>
        public string Steps { get; set; }

        /// <summary>
        /// Recipe's photo
        /// </summary>
        public string ImageLink { get; set; }
    }
}
