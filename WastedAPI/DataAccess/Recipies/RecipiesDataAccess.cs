﻿using System.Collections.Generic;
using WastedAPI.Models;


namespace WastedAPI.DataAccess.Recipies
{
    public class RecipiesDataAccess : IRecipiesDataAccess
    {
        private static List<Recipe> recipies = FakeDatabase.GetRecipes();

        List<Recipe> IRecipiesDataAccess.Recipies => recipies;

        public void Reset()
        {
            recipies = FakeDatabase.GetRecipes();
        }
    }
}
