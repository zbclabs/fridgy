﻿using Microsoft.AspNetCore.Mvc;
using WastedAPI.DataAccess.User;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WastedAPI.Controllers
{
    [Route("api/[controller]")]
    public class UserPointsController : Controller
    {
        private readonly IUserDataAccess userDataAccess = new UserDataAccess();

        /// <summary>
        /// Get user points.
        /// </summary>
        /// <returns></returns>
        // GET: api/userPoints
        [HttpGet]
        public int GetUserPoints()
        {
            return userDataAccess.Points;
        }

        /// <summary>
        /// Update user points (provide positive value to add points, negative to subtract).
        /// </summary>
        /// <param name="difference"></param>
        /// <returns></returns>
        // POST: api/userPoints
        [HttpPost]
        public int UpdateUserPoints([FromBody] int difference)
        {
            userDataAccess.Points += difference;
            return userDataAccess.Points;
        }
    }
}
